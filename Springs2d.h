#ifndef _SPRINGS_2D_H_
#define _SPRINGS_2D_H_

#include <QtGui>
#include <ControlWidget.h>
#include <SimDisplay.h>
#include <QThread>
#include <Integrator.h>


class Refresher : public QThread{
  public:
    Refresher(Simulation * sim);
    void run();

  private:
    Simulation * sim_;
};


class Drawer : public QThread{
  public:
    Drawer(SimDisplay * display);
    void run();

  private:
    SimDisplay * display_;
};


class Springs2d : public QMainWindow{
  Q_OBJECT

  public:
    Springs2d();   
    virtual ~Springs2d();

  public slots:
    void reset();

  private:
    ControlWidget * settings_widget_;
    SimDisplay * display_;
    
    Simulation * simulation_;
    Integrator * rk4_;
    Integrator * euler_;

    // background threads
    Refresher * refresher_;
    Drawer * drawer_;
};



#endif
