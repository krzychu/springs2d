#include <Springs2d.h>
#include <BorderLayout.h>
#include <iostream>
#include <utility>

Springs2d::Springs2d()
{
  // generate models
  simulation_ = new Simulation;
  rk4_ = new RK4Integrator;
  euler_ = new EulerIntegrator;
  simulation_->set_integrator(rk4_);

  ControlWidget::integrator_list ints;
  ints.push_back(std::pair<const char *, Integrator *>("RK4", rk4_));
  ints.push_back(std::pair<const char *, Integrator *>("Euler", euler_));

  // generate gui 
  display_ = new SimDisplay(simulation_, this);
  settings_widget_ = new ControlWidget(simulation_, display_, ints, this);

  
  BorderLayout * layout = new BorderLayout;
  layout->addWidget(settings_widget_, BorderLayout::West);
  layout->addWidget(display_, BorderLayout::Center);

  connect(settings_widget_, SIGNAL(reset_simulation()), this,
      SLOT(reset()));

  setCentralWidget(new QWidget(this));
  QWidget * center = centralWidget();
  center->setLayout(layout);

  setWindowTitle("Springs2d");

  // background threads
  refresher_ = new Refresher(simulation_);
  refresher_->start();
  drawer_ = new Drawer(display_);
  drawer_->start();
}


Springs2d::~Springs2d(){


  refresher_->terminate();
  refresher_->wait();
  delete refresher_;

  drawer_->terminate();
  drawer_->wait();
  delete drawer_;

  delete simulation_;
  delete rk4_;
  delete euler_;
}


void Springs2d::reset(){
  simulation_->reset();
}




Refresher::Refresher(Simulation * sim){
  sim_ = sim;
}

void Refresher::run(){
  while(true){
    sim_->advance(0.01);
    msleep(10);
  }
}



Drawer::Drawer(SimDisplay * display){
  display_ = display;
}

void Drawer::run(){
  while(true){
    display_->update();
    msleep(20);
  }
}
