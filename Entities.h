#ifndef _ENTITIES_H_
#define _ENTITIES_H_

#include <Vectors.h>
#include <cstddef>

struct EntityState{
  V2 position;
  V2 velocity;

  void accumulate(const EntityState & state, double f);
};


class Entity{
  public:
    Entity(int number = -1, VN<EntityState> ** parent = NULL);
    virtual ~Entity();
    
    // copying
    Entity(const Entity & ent);
    const Entity & operator=(const Entity & other);

    // getters
    const V2 & position() const;
    const V2 & velocity() const;
    const V2 & force() const;

    // setters
    void set_position(const V2 & pos);
    void set_velocity(const V2 & vel);
    void set_force(const V2 & force);

    // other accessors
    void add_force(const V2 & force);

    // calculates entity derivative
    virtual EntityState derivative(double mass, double dumping) const = 0;

  protected:
    EntityState & entity_state() const;

  private:
    int number_;
    VN<EntityState> ** parent_;
    EntityState * state_;
    V2 force_;
};


class Ball : public Entity{
  public:
    Ball(V2 pos, int number, VN<EntityState> ** parent);
    virtual EntityState derivative(double mass, double dumping) const;

    void set_static(bool s);
  private:
    bool is_static_;
};


class Fixture : public Entity{
  public:
    Fixture(const V2 & pos);
    virtual EntityState derivative(double mass, double dumping) const;
};


class Spring{
  public:
    Spring(Entity * a, Entity * b);
    void act(double constant) const;
    double initial_length() const;

    const V2 & end_a() const;
    const V2 & end_b() const;

  private:
    Entity * a_;
    Entity * b_;
    double length_;
};

#endif
