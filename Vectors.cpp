#include <Vectors.h>
#include <cmath>

V2::V2(){
  set(0,0);
}

V2::V2(double sx, double sy){
  set(sx, sy);
}

V2::V2(const V2 & other){
  set(other.x, other.y);
}

const V2 V2::operator + (const V2 & other) const{
  return V2(x + other.x, y + other.y);
}

const V2 V2::operator - (const V2 & other) const{
  return V2(x - other.x, y - other.y);
}

double V2::operator * (const V2 & other) const{
  return x*other.x + y*other.y;
}

const V2 V2::operator * (double s) const{
  return V2(s * x, s * y);
}

const V2 & V2::operator = (const V2 & other){
  set(other.x, other.y);
  return other;
}

void V2::set(double sx, double sy){
  x = sx;
  y = sy;
}

double V2::length() const{
  return sqrt(x*x + y*y);
}

void V2::accumulate(const V2 & other, double f){
  x += other.x * f;
  y += other.y * f;
}

const V2 operator * (double s, const V2 & vector){
  return vector * s;
}

