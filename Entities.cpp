#include <Entities.h>
#include <iostream>

void EntityState::accumulate(const EntityState & state, double f){
  position.accumulate(state.position, f);
  velocity.accumulate(state.velocity, f);
}


Entity::Entity(int number, VN<EntityState> ** parent){
  parent_ = parent;
  number_ = number;
  state_ = NULL;

  if(parent_ == NULL){
    // we have to create our own state
    state_ = new EntityState;
  }
}


Entity::~Entity(){
  delete state_;
}

Entity::Entity(const Entity & ent){
  number_ = ent.number_;
  force_ = ent.force_;
  parent_ = ent.parent_;
  state_ = NULL;
  if(ent.state_ != NULL){
    state_ = new EntityState();
    *state_ = *ent.state_;
  }
}


const Entity & Entity::operator=(const Entity & ent){
  number_ = ent.number_;
  force_ = ent.force_;
  parent_ = ent.parent_;
  if(ent.state_ != NULL){
    state_ = new EntityState();
    *state_ = *ent.state_;
  }
  return ent;
}

EntityState & Entity::entity_state() const{
  if(parent_ == NULL)
    return *state_;
  else
    return (**parent_)[number_];
}


// Getters

const V2 & Entity::position() const{
  return entity_state().position;
}

const V2 & Entity::velocity() const{
  return entity_state().velocity;
}

const V2 & Entity::force() const{
  return force_;
}

// Setters

void Entity::set_position(const V2 & pos){
  entity_state().position = pos;
}

void Entity::set_velocity(const V2 & vel){
  entity_state().velocity = vel;
}

void Entity::set_force(const V2 & force){
  force_ = force;
}

// Other
void Entity::add_force(const V2 & force){
  force_.accumulate(force, 1.0);
}



// Ball
Ball::Ball(V2 pos, int number, VN<EntityState> ** parent)
  : Entity(number, parent)
{
  set_position(pos);
  is_static_ = false;
}

void Ball::set_static(bool s){
  is_static_ = s;
}

EntityState Ball::derivative(double mass, double dumping) const{
  EntityState d;
  if(!is_static_){
    d.position = velocity();
    d.velocity = (1.0 / mass) * (force() - dumping * velocity());
  }
  return d;
}


// Fixture
Fixture::Fixture(const V2 & pos){
  set_position(pos);
}

EntityState Fixture::derivative(double mass, double dumping) const{
  EntityState d;
  return d;
}


// Spring
Spring::Spring(Entity * a, Entity * b){
  a_ = a;
  b_ = b;
  length_ = (a->position() - b->position()).length();
}

void Spring::act(double constant) const{
  V2 from_a_to_b = b_->position() - a_->position();
  double dist = from_a_to_b.length();
  if(dist < 0.1) 
    return;
  double delta = dist - length_;
  double force = -delta * constant / dist;
  
  a_->add_force((-force) * from_a_to_b);
  b_->add_force((force) * from_a_to_b);
}

double Spring::initial_length() const{
  return length_;
}

const V2 & Spring::end_a() const {
  return a_->position();
}

const V2 & Spring::end_b() const {
  return b_->position();
}
