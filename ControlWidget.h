#ifndef _SETTINGS_WIDGET_H_
#define _SETTINGS_WIDGET_H_

#include <QtGui>
#include <Simulation.h>
#include <SimDisplay.h>
#include <Integrator.h>


class IntegratorSettings : public QGroupBox{
  Q_OBJECT;

  public:
    typedef std::vector<std::pair<const char *, Integrator *> > integrator_list;
    IntegratorSettings(Simulation * sim, const integrator_list & integrators);

  public slots:
    void select_item(int item);

  private:
    QComboBox * combo_;
    integrator_list integrators_;
    Simulation * sim_;
};



class BallSettings : public QGroupBox{
    Q_OBJECT
  
  public:
    BallSettings(Simulation * settings = NULL, QWidget * parent = NULL);

  public slots:
    void set_mass(double mass);
    void set_number(int n);
    void set_dumping(double d);

  private:
    QDoubleSpinBox * dumping_;
    QDoubleSpinBox * mass_;    
    QSpinBox * number_;
    Simulation * simulation_;
};



class SpringSettings : public QGroupBox{
    Q_OBJECT
  
  public:
    SpringSettings(Simulation * settings = NULL, QWidget * parent = NULL);

  public slots:
    void set_k(double k);

  private:
    QDoubleSpinBox * k_;
    Simulation * simulation_;
};


class ViewSettings : public QGroupBox{
    Q_OBJECT

  public:
    ViewSettings(SimDisplay * display, QWidget * parent = NULL);

  private:
    QDoubleSpinBox * zoom_;
    QDoubleSpinBox * radius_;
    QCheckBox * visible_;
};


class ControlWidget : public QWidget{
    Q_OBJECT

  public:
    typedef typename IntegratorSettings::integrator_list integrator_list;
    ControlWidget(Simulation * settings, SimDisplay * display,
          const integrator_list & ints, QWidget * parent = NULL);

  signals:
    void reset_simulation();

  public slots:
    void reset_clicked();

  private:
    QLabel * title_;
    BallSettings * balls_box_;
    SpringSettings * springs_box_;
    IntegratorSettings * integrator_box_;
    ViewSettings * view_box_;
    QPushButton * reset_button_;
    Simulation * simulation_;
};
#endif

