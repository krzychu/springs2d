#ifndef _SIMULATION_H_
#define _SIMULATION_H_

#include <Vectors.h>
#include <Integrator.h>
#include <Entities.h>
#include <QMutex>



class Simulation : public Integrable{
  public:
    Simulation();

    // Integration
    void set_integrator(Integrator * integrator);
    void advance(double dt);
    
    // Integrable interface
    virtual void state_copy(VN<EntityState> * state);
    virtual void set_state(VN<EntityState> * state);
    virtual void derivative(double t, VN<EntityState> * state, 
      VN<EntityState> * derivative);
  
    // Accessors

    std::vector<Ball*>::iterator balls_begin();
    std::vector<Ball*>::iterator balls_end();
    std::vector<Spring*>::iterator springs_begin();
    std::vector<Spring*>::iterator springs_end();
    std::vector<Fixture*>::iterator fixtures_begin();
    std::vector<Fixture*>::iterator fixtures_end();

    double ball_mass() const;
    int num_balls() const;
    double spring_constant() const;
    double dumping() const;

    void set_ball_mass(double m);
    void set_num_balls(int n);
    void set_spring_constant(double k);
    void set_dumping(double k);

    // concurrency
    void lock();
    void unlock();
    void reset();

    // default values
    static const double max_ball_mass;
    static const double min_ball_mass;
    static const double default_ball_mass;

    static const int max_num_balls;
    static const int min_num_balls;
    static const int default_num_balls;

    static const double max_spring_constant;
    static const double min_spring_constant;
    static const double default_spring_constant;

    static const double max_dumping;
    static const double min_dumping;
    static const double default_dumping;

    static const double spring_length;

    // Destructor
    virtual ~Simulation();


  private:
    // physics constants
    double ball_mass_;
    int num_balls_;

    double spring_constant_;
    double dumping_;

    // world elements
    std::vector<Fixture*> fixtures_;
    std::vector<Ball*> balls_;
    std::vector<Spring*> springs_;

    // integration
    Integrator * integrator_;
    double t_;
    VN<EntityState> * state_; 
    QMutex mutex_;

    // synchronizes reality with settings
    void sync_with_settings();
    void clear_entities();
};

#endif
