#include <Springs2d.h>

#include <QApplication>

int main(int argc, char ** argv){
 
  QApplication app(argc, argv);
  QApplication::setStyle(new QPlastiqueStyle);
  Springs2d sp2d;
  sp2d.show();

  return app.exec();
}
