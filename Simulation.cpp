#include <Simulation.h>
#include <algorithm>
#include <iostream>

Simulation::Simulation(){
  t_ = 0.0;
  integrator_ = NULL;
  state_ = NULL;  

  ball_mass_ = default_ball_mass;
  num_balls_ = default_num_balls;

  spring_constant_ = default_spring_constant;
  dumping_ = default_dumping;

  sync_with_settings();
}


// Internal
void Simulation::sync_with_settings(){  
  lock();
  // remove current sim
  clear_entities();
  delete state_;
  t_ = 0.0;

  double d = spring_length;
  int n = num_balls_;

  // ball state
  state_ = new VN<EntityState>(n*n);

  // balls and fixtures
  int ball_num = 0;
  double cur_y = - d * double(n / 2);
  double cur_x = - d * double(n / 2);

  // upper fixtures
  for(int x = 0; x < n; x++){
    fixtures_.push_back(new Fixture(V2(cur_x, cur_y - d)));
    cur_x += d;
  }

  Entity * last;
  Entity * next;
  for(int y = 0; y < n; y++){
    cur_x = - d * double(n / 2);

    // add leftmost fixture
    fixtures_.push_back(new Fixture(V2(cur_x - d, cur_y)));
    last = fixtures_.back();

    for(int x = 0; x < n; x++){
      // add ball
      balls_.push_back(new Ball(V2(cur_x, cur_y), ball_num++, &state_));
      next = balls_.back();

      // add spring
      springs_.push_back(new Spring(last,next));
      last = next;

      cur_x += d;
    }
    fixtures_.push_back(new Fixture(V2(cur_x, cur_y)));
    springs_.push_back(new Spring(balls_.back(), fixtures_.back()));

    cur_y += d;
  }

  //  fixtures
  cur_x = - d * double(n / 2);
  for(int x = 0; x < n; x++){
    fixtures_.push_back(new Fixture(V2(cur_x, cur_y)));
    cur_x += d;
  }

  // vertical springs
  for(int x = 0; x < n ; x++){
    last = fixtures_[x];
    for(int y = 0 ; y < n ; y++){
      springs_.push_back(new Spring(last, balls_[x + n*y]));
      last = balls_[x + n * y];
    }
    springs_.push_back(new Spring(last, fixtures_[x + 3*n]));
  }
  unlock();
}



void Simulation::advance(double dt){
  lock();
  integrator_->integrate(t_, dt, *this);
  t_ += dt;
  unlock();
}


// Integrable interface

void Simulation::state_copy(VN<EntityState> * state){
  state->clear();
  std::copy(state_->begin(), state_->end(), 
    std::back_inserter(*state));
}

void Simulation::set_state(VN<EntityState> * state){
  delete state_;
  state_ = state;
}

void Simulation::derivative(double t, VN<EntityState> * state, 
      VN<EntityState> * derivative)
{
  std::swap(state, state_);

  // set ball forces to 0
  std::vector<Ball*>::iterator ball;
  for(ball = balls_.begin(); ball != balls_.end(); ball++)
    (*ball)->set_force(V2(0,0));

  // process springs
  std::vector<Spring*>::iterator spring;
  for(spring = springs_.begin(); spring != springs_.end(); spring++)
    (*spring)->act(spring_constant_);
  
  // compute derivatives
  derivative->clear();
  for(ball = balls_.begin(); ball != balls_.end(); ball++){
    derivative->push_back((*ball)->derivative(ball_mass_, dumping_));
  }

  std::swap(state, state_);
}
 

// Accessors
void Simulation::set_integrator(Integrator * i){
  lock();
  integrator_ = i;
  unlock();
}

std::vector<Ball*>::iterator Simulation::balls_begin(){
  return balls_.begin();
}

std::vector<Ball*>::iterator Simulation::balls_end(){
  return balls_.end();
}

std::vector<Spring*>::iterator Simulation::springs_begin(){
  return springs_.begin();
}

std::vector<Spring*>::iterator Simulation::springs_end(){
  return springs_.end();
}

std::vector<Fixture*>::iterator Simulation::fixtures_begin(){
  return fixtures_.begin();
}

std::vector<Fixture*>::iterator Simulation::fixtures_end(){
  return fixtures_.end();
}


double Simulation::ball_mass() const{
  return ball_mass_;
}

int Simulation::num_balls() const{
  return num_balls_;
}

double Simulation::spring_constant() const{
  return spring_constant_;
}

double Simulation::dumping() const{
  return dumping_;
}

// setters
void Simulation::set_ball_mass(double m){
  lock();
  ball_mass_ = m;
  unlock();
}

void Simulation::set_num_balls(int n){
  num_balls_ = n;
  sync_with_settings();
}

void Simulation::set_spring_constant(double k){
  lock();
  spring_constant_ = k;
  unlock();
}

void Simulation::set_dumping(double d){
  lock();
  dumping_ = d;
  unlock();
}


// Destructor
Simulation::~Simulation(){
  clear_entities();
  delete state_;  
}

void Simulation::clear_entities(){
  std::vector<Ball*>::iterator ball;
  for(ball = balls_.begin(); ball != balls_.end(); ball++)
    delete *ball;
  balls_.clear();

  std::vector<Fixture*>::iterator fixture;
  for(fixture = fixtures_.begin(); fixture != fixtures_.end(); fixture++)
    delete *fixture;
  fixtures_.clear();

  std::vector<Spring*>::iterator spring;
  for(spring = springs_.begin(); spring != springs_.end(); spring++)
    delete *spring;
  springs_.clear();
}

void Simulation::lock(){
  mutex_.lock();
}

void Simulation::unlock(){
  mutex_.unlock();
}

void Simulation::reset(){
  sync_with_settings();
}

const double Simulation::max_ball_mass = 10.0;
const double Simulation::min_ball_mass = 1.0;
const double Simulation::default_ball_mass = 5.0;

const int Simulation::max_num_balls = 200;
const int Simulation::min_num_balls = 1;
const int Simulation::default_num_balls  = 5;

const double Simulation::max_spring_constant = 50.0;
const double Simulation::min_spring_constant = 1.0;
const double Simulation::default_spring_constant = 20.0;

const double Simulation::max_dumping = 50.0;
const double Simulation::min_dumping = 0.0;
const double Simulation::default_dumping = 0.0;

const double Simulation::spring_length = 100.0;
