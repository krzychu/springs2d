#include <Integrator.h>
#include <iostream>

RK4Integrator::RK4Integrator(){
  a[0] = 0.0;
  a[1] = 1.0 / 2.0;
  a[2] = 1.0 / 2.0;
  a[3] = 1.0;

  c[0] = 1.0 / 6.0;
  c[1] = 1.0 / 3.0;
  c[2] = 1.0 / 3.0;
  c[3] = 1.0 / 6.0;

  for(int x = 0; x < 4; x++)
    for(int y = 0; y < 4; y++)
      b[x][y] = 0.0;

  b[1][0] = 1.0 / 2.0;
  b[2][1] = 1.0 / 2.0;
  b[3][2] = 1.0;
}



void RK4Integrator::integrate(double t, double dt, Integrable & sim)
{
  VN<EntityState> * k[4];
  VN<EntityState> * final_state = new VN<EntityState>;
  sim.state_copy(final_state);

  VN<EntityState> cur_state;
  for(int i = 0; i < 4; i++){
    // determine point of evaluation
    double cur_t = t + a[i] * dt;
    sim.state_copy(&cur_state);
    for(int j = 0; j < i; j++){
      cur_state.accumulate(k[j],dt*b[i][j]);
    }
    
    k[i] = new VN<EntityState>;
    sim.derivative(cur_t, &cur_state, k[i]);

    final_state->accumulate(k[i], dt*c[i]);
  }  

  sim.set_state(final_state);
  
  for(int i = 0; i < 4; i++)
    delete k[i];
}
 

void EulerIntegrator::integrate(double t, double dt, Integrable & sim){
  VN<EntityState> * state = new VN<EntityState>;
  VN<EntityState> * d = new VN<EntityState>;
  sim.state_copy(state);
  sim.derivative(t, state, d);
  state->accumulate(d, dt);
  sim.set_state(state);
}
