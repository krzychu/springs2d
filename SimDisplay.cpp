#include <SimDisplay.h>
#include <iostream>
#include <cmath>

const int SimDisplay::default_height = 600;
const int SimDisplay::default_width = 800;
const int SimDisplay::num_ball_faces = 200;

const double SimDisplay::default_zoom = 100.0;
const double SimDisplay::max_zoom = 1000.0;
const double SimDisplay::min_zoom = 1.0;

const double SimDisplay::max_ball_radius = 300.0;
const double SimDisplay::min_ball_radius = 20.0;
const double SimDisplay::default_ball_radius = 30.0;

const bool SimDisplay::default_springs_visible = true;


SimDisplay::SimDisplay(Simulation * sim, QWidget * parent)
  : QGLWidget(parent)
{
  setFormat(QGLFormat(QGL::DoubleBuffer | QGL::SampleBuffers));
  simulation_ = sim;
  zoom_ = default_zoom;
  ball_list_ = -1;
  spring_list_ = -1;
  num_ball_faces_ = 50;
  selected_ball_ = NULL;
  springs_visible_ = default_springs_visible;
  ball_radius_ = default_ball_radius;
}


// GL context management
QSize SimDisplay::minimumSizeHint() const{
  return QSize(default_width, default_height);
}


QSize SimDisplay::sizeHint() const{
  return QSize(default_width, default_height);
}

void SimDisplay::initializeGL(){
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

  if(ball_list_ != -1)
    glDeleteLists(ball_list_, 1);

  if(spring_list_ != -1)
    glDeleteLists(spring_list_, 1);

  ball_list_ = glGenLists(2);
  spring_list_ = ball_list_ + 1;

  // compile ball list
  glNewList(ball_list_, GL_COMPILE);
    float alpha = 2.0 * M_PI / float(num_ball_faces_);
    glBegin(GL_TRIANGLE_FAN);
    glVertex2f(0.0, 0.0);
    for (int face = 0; face <= num_ball_faces_; face++) {
      glVertex2f(cos(face * alpha), sin(face * alpha));
    }
    glEnd();
  glEndList();

  // enable anitaliasing
  glEnable(GL_MULTISAMPLE);
}

void SimDisplay::paintGL(){
  simulation_->lock();
  // clear background
  glClear(GL_COLOR_BUFFER_BIT);

  // draw balls
  std::vector<Ball*>::const_iterator ball;
  for(ball = simulation_->balls_begin();
      ball != simulation_->balls_end(); ball++)
  {
    if(*ball != selected_ball_)
      glColor3f(0,1,0);
    else
      glColor3f(1,0,0);
    draw_ball(**ball);
  }

  // draw fixtures
  glColor3f(0,0,1);
  std::vector<Fixture*>::const_iterator fix;
  for(fix = simulation_->fixtures_begin();
      fix != simulation_->fixtures_end(); fix++)
  {
    draw_fixture(**fix);
  }

  // draw springs
  glColor3f(1,0,0);
  std::vector<Spring*>::const_iterator spring;
  for(spring = simulation_->springs_begin();
      spring != simulation_->springs_end(); spring++)
  {
    draw_spring(**spring);
  }
  simulation_->unlock();
}


void SimDisplay::update_projection(){
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  double w = width() / zoom_ * 50.0;
  double h = height() / zoom_ * 50.0;

  V2 c = camera_position_;

  glOrtho(-w + c.x, w + c.x, h + c.y, -h + c.y, -1, 1);
}

void SimDisplay::resizeGL(int w, int h){
  glViewport(0,0,w,h);

  update_projection();

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void SimDisplay::draw_harmonica(double width, double height, int num_breaks){
  double break_h = height / double(num_breaks);
  double y = 0;
  glBegin(GL_LINE_STRIP);
  glVertex2f(0,0);
  for(int i = 0; i < num_breaks; i ++){
    glVertex2f(-width / 2.0, y + 0.25 * break_h);
    glVertex2f(width / 2.0, y + 0.75 * break_h);
    glVertex2f(0, y + break_h);
    y += break_h;
  }
  glEnd();
}


void SimDisplay::draw_ball(const Ball & ball){
  glPushMatrix();
  glTranslatef(ball.position().x, ball.position().y, 0);
  float f = ball_radius_;
  glScalef(f,f,f);
  glCallList(ball_list_);
  glPopMatrix();
}


void SimDisplay::draw_fixture(const Fixture & fix){
  glPushMatrix();
  glTranslatef(fix.position().x, fix.position().y, 0);
  float f = ball_radius_ / 5.0;
  glScalef(f,f,f);
  glCallList(ball_list_);
  glPopMatrix();
}


void SimDisplay::draw_spring(const Spring & spring){
  V2 a = spring.end_a();
  V2 b = spring.end_b();
  glBegin(GL_LINES);
    glVertex2f(a.x, a.y);
    glVertex2f(b.x, b.y);
  glEnd();
}


double SimDisplay::zoom() const{
  return zoom_;
}

void SimDisplay::set_zoom(double zoom){
  zoom_ = zoom;
  update_projection();
}

// Position manipulation
V2 SimDisplay::simulation_event_pos(QMouseEvent * ev) const{
  double x = (ev->pos().x() - width() / 2.0 )* 100.0 / zoom_;
  double y = (ev->pos().y() -  height() / 2.0)* 100.0 / zoom_;
  V2 p(x,y);
  return p + camera_position_;
}

V2 SimDisplay::scale_to_simulation(const V2 & p) const{
  return p * (100.0 / zoom_);
}

V2 SimDisplay::event_to_vector(QMouseEvent * ev) const{
  return V2(ev->pos().x(), ev->pos().y());
}


//  Mouse
void SimDisplay::mousePressEvent(QMouseEvent * event){
  setMouseTracking(true);
  last_mouse_position_ = scale_to_simulation(event_to_vector(event));

  // try to determine which ball was clicked
  simulation_->lock();
  V2 pos = simulation_event_pos(event);
  std::vector<Ball*>::iterator itr;
  for(itr = simulation_->balls_begin();
      itr != simulation_->balls_end(); itr++)
  {
    double dist = (pos - (*itr)->position()).length();
    if(dist < ball_radius_){
      selected_ball_ = *itr;
      selected_ball_->set_static(true);
      break;
    }
  }
  simulation_->unlock();
}


void SimDisplay::mouseReleaseEvent(QMouseEvent * event){
  setMouseTracking(false);
  if(selected_ball_ != NULL){
    simulation_->lock();
    selected_ball_->set_static(false);
    simulation_->unlock();
  }
  selected_ball_ = NULL;
}


void SimDisplay::mouseMoveEvent(QMouseEvent * event){
  V2 mouse_position = scale_to_simulation(event_to_vector(event));
  V2 delta = mouse_position - last_mouse_position_;

  if(selected_ball_ == NULL){
    camera_position_ = camera_position_ - delta;
    update_projection();
  }
  else{
    selected_ball_->set_position(selected_ball_->position() + delta);
  }

  last_mouse_position_ = mouse_position;
}


void SimDisplay::set_springs_visible(bool v){
  springs_visible_ = v;
}

void SimDisplay::set_ball_radius(double r){
  ball_radius_ = r;
}

bool SimDisplay::springs_visible() const{
  return springs_visible_;
}

double SimDisplay::ball_radius() const{
  return ball_radius_;
}
