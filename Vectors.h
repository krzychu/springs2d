#ifndef _VECTORS_H_
#define _VECTORS_H_

#include <vector>


template<class T>
class VN : public std::vector<T>{
  public:
    VN(){}
    VN(const VN<T> & other) : std::vector<T>(other){}
    VN(const VN<T> * other) : std::vector<T>(*other){}
    VN(int initial_size) : std::vector<T>(initial_size){}

    void accumulate(const VN<T> * other, double factor){
      if(factor == 0)
        return;

      for(int i = 0; i < std::vector<T>::size(); i++){
        (*this)[i].accumulate((*other)[i], factor);
      }
    }
};




struct V2{
  
  V2();
  V2(double sx, double sy);
  V2(const V2 & other);

  // sum
  const V2 operator + (const V2 & other) const;
  
  // diffrence
  const V2 operator - (const V2 & other) const;
  
  // scalar product
  double operator * (const V2 & other) const;

  // multiply by constant
  const V2 operator * (double s) const;

  const V2 & operator = (const V2 & other);

  // length
  double length() const;
  
  void accumulate(const V2 & other, double factor);

  void set(double sx, double sy);

  double x;
  double y;
};

const V2 operator * (double s, const V2 & vector);

#endif

