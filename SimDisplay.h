#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#include <QtGui>
#include <QGLWidget>
#include <Simulation.h>

class SimDisplay : public QGLWidget {
  Q_OBJECT

  public:

    SimDisplay(Simulation * sim, QWidget * parent = NULL);
    QSize minimumSizeHint() const;
    QSize sizeHint() const;

    double zoom() const;
    bool springs_visible() const;
    double ball_radius() const;

    const static int default_width;
    const static int default_height;
    const static int num_ball_faces;
    static const bool default_springs_visible;

    static const double max_ball_radius;
    static const double min_ball_radius;
    static const double default_ball_radius;

    const static double default_zoom;
    const static double max_zoom;
    const static double min_zoom;

  public slots:
    void set_zoom(double zoom);
    void set_springs_visible(bool v);
    void set_ball_radius(double d);

  protected:
    void mousePressEvent(QMouseEvent * event);
    void mouseReleaseEvent(QMouseEvent * event);
    void mouseMoveEvent(QMouseEvent * event);

  private:
    // opengl context management
    void initializeGL();
    void paintGL();
    void resizeGL(int w, int h);
    void update_projection();
    int ball_list_;
    int spring_list_;

    V2 simulation_event_pos(QMouseEvent * event) const;
    V2 scale_to_simulation(const V2 & p) const;
    V2 event_to_vector(QMouseEvent * event) const;

    // drawers
    void draw_harmonica(double width, double height, int num_breaks);
    void draw_ball(const Ball & ball);
    void draw_fixture(const Fixture & fixture);
    void draw_spring(const Spring & spring);

    Simulation * simulation_;
    int state_;
    V2 last_mouse_position_;
    Ball * selected_ball_;

    // general settings
    int num_ball_faces_;
    bool springs_visible_;
    double ball_radius_;

    // view settings
    double zoom_;
    V2 camera_position_;


};

#endif
