#include <ControlWidget.h>
#include <iostream>

ControlWidget::ControlWidget(Simulation * settings, SimDisplay * display,
    const integrator_list & ints, QWidget * parent)
: QWidget(parent), simulation_(settings)
{
  
  reset_button_ = new QPushButton("Reset");

  balls_box_ = new BallSettings(simulation_);
  springs_box_ = new SpringSettings(simulation_);
  view_box_ = new ViewSettings(display);
  integrator_box_ = new IntegratorSettings(simulation_, ints);

  // top level
  QVBoxLayout * layout = new QVBoxLayout;
  layout->addWidget(view_box_);
  layout->addWidget(balls_box_);
  layout->addWidget(springs_box_);
  layout->addWidget(integrator_box_);
  layout->addWidget(reset_button_);
  setLayout(layout);

  connect(reset_button_, SIGNAL(clicked()), this,
      SLOT(reset_clicked()));
}

void ControlWidget::reset_clicked(){
  emit reset_simulation();
}




BallSettings::BallSettings(Simulation* settings, QWidget * parent)
  : QGroupBox("Ball settings",parent), simulation_(settings)
{
  mass_ = new QDoubleSpinBox;
  mass_->setMinimum(Simulation::min_ball_mass);
  mass_->setMaximum(Simulation::max_ball_mass);
  mass_->setValue(Simulation::default_ball_mass);
  
  dumping_ = new QDoubleSpinBox;
  dumping_->setMinimum(Simulation::min_dumping);
  dumping_->setMaximum(Simulation::max_dumping);
  dumping_->setValue(Simulation::default_dumping);

  number_ = new QSpinBox;
  number_->setMinimum(Simulation::min_num_balls);
  number_->setMaximum(Simulation::max_num_balls);
  number_->setValue(Simulation::default_num_balls);

  QFormLayout * layout = new QFormLayout;
  layout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
  layout->addRow("Mass", mass_);
  layout->addRow("Dumping", dumping_);
  layout->addRow("N = ",number_);
  setLayout(layout);

  connect(mass_, SIGNAL(valueChanged(double)), 
    this, SLOT(set_mass(double)));
  connect(dumping_, SIGNAL(valueChanged(double)),
    this, SLOT(set_dumping(double)));
  connect(number_, SIGNAL(valueChanged(int)), 
    this, SLOT(set_number(int)));

}

void BallSettings::set_mass(double mass){
  simulation_->set_ball_mass(mass);
}

void BallSettings::set_number(int num){
  simulation_->set_num_balls(num);
}

void BallSettings::set_dumping(double dumping){
  simulation_->set_dumping(dumping);
}



SpringSettings::SpringSettings(Simulation* settings, QWidget * parent)
  : QGroupBox("Spring settings",parent), simulation_(settings)
{
  k_ = new QDoubleSpinBox;
  k_->setMinimum(Simulation::min_spring_constant);
  k_->setMaximum(Simulation::max_spring_constant);
  k_->setValue(Simulation::default_spring_constant);


  QFormLayout * layout = new QFormLayout;
  layout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
  layout->addRow("K = ", k_);
  setLayout(layout);

  connect(k_, SIGNAL(valueChanged(double)), 
    this, SLOT(set_k(double)));
}

void SpringSettings::set_k(double k){
  simulation_->set_spring_constant(k);
}


ViewSettings::ViewSettings(SimDisplay * display, QWidget * parent)
  : QGroupBox("View settings",parent)
{
  zoom_ = new QDoubleSpinBox;
  zoom_->setMinimum(SimDisplay::min_zoom);
  zoom_->setMaximum(SimDisplay::max_zoom);
  zoom_->setValue(SimDisplay::default_zoom);

  radius_ = new QDoubleSpinBox;
  radius_->setMinimum(SimDisplay::min_ball_radius);
  radius_->setMaximum(SimDisplay::max_ball_radius);
  radius_->setValue(SimDisplay::default_ball_radius);

  visible_ = new QCheckBox("Display springs");
  if(SimDisplay::default_springs_visible)
    visible_->setCheckState(Qt::Checked);
  else
    visible_->setCheckState(Qt::Unchecked);


  QFormLayout * layout = new QFormLayout;
  layout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
  layout->addRow("Zoom", zoom_);
  layout->addRow("Ball radius", radius_);
  //layout->addRow(visible_);
  setLayout(layout);

  connect(zoom_, SIGNAL(valueChanged(double)),
    display, SLOT(set_zoom(double)));
  connect(radius_, SIGNAL(valueChanged(double)),
    display, SLOT(set_ball_radius(double)));
}



IntegratorSettings::IntegratorSettings(Simulation * sim,
    const integrator_list & integrators)
  : QGroupBox("Integrator")
{
  integrators_ = integrators;
  combo_ = new QComboBox;
  sim_ = sim;

  integrator_list::const_iterator itr;
  for(itr = integrators.begin(); itr != integrators.end(); itr++){
    combo_->addItem(itr->first);
  }

  QHBoxLayout * layout = new QHBoxLayout;
  layout->addWidget(combo_);
  setLayout(layout);

  connect(combo_, SIGNAL(currentIndexChanged(int)), this,
      SLOT(select_item(int)));
}

void IntegratorSettings::select_item(int index){
  sim_->set_integrator(integrators_[index].second);
}
