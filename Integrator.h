#ifndef _INTEGRATOR_H_
#define _INTEGRATOR_H_

#include <Vectors.h>
#include <Entities.h>
#include <QObject>

class Integrable{

  public:
    // copies simulation state to given vector
    virtual void state_copy(VN<EntityState> * state) = 0;

    // sets simulation state and transfers its ownership
    virtual void set_state(VN<EntityState> * state) = 0;

    // evaluates state derivative at given point and copies it into
    // derivative vector
    virtual void derivative(double t, VN<EntityState> * state, 
      VN<EntityState> * derivative) = 0;

    virtual ~Integrable(){}
};



class Integrator{
  public:
    virtual void integrate(double t, double dt, Integrable & sim) = 0;
};


class EulerIntegrator : public Integrator{
  public:
    virtual void integrate(double t, double dt, Integrable & sim);
    virtual ~EulerIntegrator(){};
};



class RK4Integrator : public Integrator{
  public:
    RK4Integrator();
    virtual void integrate(double t, double dt, Integrable & sim);
    virtual ~RK4Integrator(){};

  private:
    double a[4];
    double c[4];
    double b[4][4];
};

 
#endif
